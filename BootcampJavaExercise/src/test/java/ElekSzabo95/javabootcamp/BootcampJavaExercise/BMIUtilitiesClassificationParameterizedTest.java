package ElekSzabo95.javabootcamp.BootcampJavaExercise;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ElekSzabo95.javabootcamp.BootcampJavaExercise.BMIUtilities.BMIClassification;

/**
 * Parameterized test class to test the BMI classification of
 * {@link ElekSzabo95.javabootcamp.BootcampJavaExercise.BMIUtilities.BMIClassification#getBMIClass(double bmiValue)}.
 * 
 * @author Elek Szabo
 *
 */
@RunWith(Parameterized.class)
public class BMIUtilitiesClassificationParameterizedTest {

	@Parameter(value = 0)
	public double bmiValueToTest;
	@Parameter(value = 1)
	public BMIClassification expectedClass;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { 10, BMIClassification.SEVERE_THINNESS },
				{ 16.5, BMIClassification.MODERATE_THINNESS }, { 18, BMIClassification.MILD_THINNESS },
				{ 20, BMIClassification.NORMAL }, { 27, BMIClassification.OVERWEIGHT },
				{ 32, BMIClassification.OBESE_CLASS_I }, { 37, BMIClassification.OBESE_CLASS_II },
				{ 43, BMIClassification.OBESE_CLASS_III } });
	}

	/**
	 * Test method for
	 * {@link ElekSzabo95.javabootcamp.BootcampJavaExercise.BMIUtilities.BMIClassification#getBMIClass(double bmiValue)}.
	 * Compares the calculated BMI class with a predefined expected value.
	 */
	@Test
	public void testBMIClassification() {
		assertEquals(expectedClass, BMIClassification.getBMIClass(bmiValueToTest));
	}

}
