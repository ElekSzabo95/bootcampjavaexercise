package ElekSzabo95.javabootcamp.BootcampJavaExercise;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 * Parameterized test class to test correct behavior of
 * {@link ElekSzabo95.javabootcamp.BootcampJavaExercise.BMIUtilities#calculateBMI(double, double)}.
 * Parameters are used to verify both happy and error path.
 * 
 * @author Elek Szabo
 *
 */
@RunWith(Parameterized.class)
public class BMIUtilitiesCalculateParameterizedTest {

	@Parameter(value = 0)
	public double weight;
	@Parameter(value = 1)
	public double height;
	@Parameter(value = 2)
	public double expectedResult;
	@Parameter(value = 3)
	public boolean illegalArgumentExpected;

	@Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { { -70, 1.7, 0, true }, // negative
																		// weight
																		// >>
																		// should
																		// throw
																		// IllegalArgumentException,
																		// result
																		// is
																		// not
																		// relevant
				{ 0, 1.5, 0, true }, // zero weight >> should throw
										// IllegalArgumentException, result is
										// not relevant
				{ 62, 0, 0, true }, // zero height >> should throw
									// IllegalArgumentException, result is not
									// relevant
				{ 112, -1.8, 0, true }, // negative height >> should throw
										// IllegalArgumentException, result is
										// not relevant
				{ 0, 0, 0, true }, // zero weight & height >> should throw
									// IllegalArgumentException, result is not
									// relevant
				{ -75, -1.78, 0, true }, // negative weight & height >> should
											// throw IllegalArgumentException,
											// result is not relevant
				{ 75, 1.81, (75.0 / (1.81 * 1.81)), false }, // valid input
				{ 63, 1.59, (63.0 / (1.59 * 1.59)), false } // valid input
		});
	}

	/**
	 * Test method for
	 * {@link ElekSzabo95.javabootcamp.BootcampJavaExercise.BMIUtilities#calculateBMI(double, double)}.
	 * Validates input parameter checks (throwing of IllegalArgumentExcpetion)
	 * and result calculation
	 */
	@Test
	public void testCalculateBMI() {
		try {
			double result = BMIUtilities.calculateBMI(weight, height);
			if (illegalArgumentExpected) {
				fail("IllegalArgumentException was not thrown for illegal parameter value. weight: " + weight
						+ " height: " + height);
			} else {
				assertEquals((Double) expectedResult, (Double) result);
			}
		} catch (IllegalArgumentException e) {
			if (illegalArgumentExpected) {
				assertTrue(true);
			} else {
				fail("IllegalArgumentException was thrown when it was not expected. weight: " + weight + " height: "
						+ height + " Exception: " + e);
			}
		}
	}

}
