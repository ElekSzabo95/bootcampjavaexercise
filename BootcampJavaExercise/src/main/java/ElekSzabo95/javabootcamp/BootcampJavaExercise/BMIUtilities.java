package ElekSzabo95.javabootcamp.BootcampJavaExercise;

/**
 * Utility class providing static methods for BMI calculation and classification
 * of BMI values. All methods are static and can be invoked without
 * instantiation of the class. All methods are working only with local variables
 * and the parameters, hence invoking the methods is thread safe and does not
 * require synchronization.
 *
 * @author Elek Szabo
 */
public class BMIUtilities {
	private static double ONE_POUND_IN_KILOGRAMS = 0.45359237;
	private static double ONE_INCH_IN_METERS = 0.0254;

	/**
	 * Private constructor to avoid instantiation.
	 */
	private BMIUtilities() {
	}

	/**
	 * Enumeration holding BMI classes constants. Each class holds it's name and
	 * the lower end of the given range. A public static method is provided to
	 * get the right class for a given BMI value.
	 *
	 */
	public enum BMIClassification {
		SEVERE_THINNESS("Severe Thinness", 0), MODERATE_THINNESS("Moderate Thinness", 16), MILD_THINNESS(
				"Mild Thinness",
				17), NORMAL("Normal", 18.5), OVERWEIGHT("Overweight", 25), OBESE_CLASS_I("Obese Class I",
						30), OBESE_CLASS_II("Obese Class II", 35), OBESE_CLASS_III("Obese Class III", 40);

		private String classificationName;
		private double lowerLimit;

		private BMIClassification(String classificationName, double lowerLimit) {
			this.classificationName = classificationName;
			this.lowerLimit = lowerLimit;
		}

		@Override
		public String toString() {
			return classificationName;
		}

		/**
		 * Determines the BMI class for a given BMI value.
		 * 
		 * @param bmiValue
		 *            The BMI value to be classified.
		 * @return The BMI class of the given BMI value.
		 */
		public static BMIClassification getBMIClass(double bmiValue) {
			BMIClassification[] bmiValues = BMIClassification.values();
			for (int i = 0; i < (bmiValues.length - 1); i++) {
				if (bmiValue < bmiValues[i + 1].lowerLimit) {
					return bmiValues[i];
				}
			}
			return OBESE_CLASS_III;
		}
	}

	/**
	 * Calculates BMI value from weight and height using metric units (kilogram
	 * and meter).
	 * 
	 * @param weightInKg
	 *            Body weight in kilograms.
	 * @param heightInMeter
	 *            Body height in meters.
	 * @return The calculated BMI value = weightInKg / (heightInMeter *
	 *         heightInMeter)
	 * @throws IllegalArgumentException
	 *             If parameters are negative or zero.
	 */
	public static double calculateBMI(double weightInKg, double heightInMeter) throws IllegalArgumentException {
		if (weightInKg <= 0) {
			throw new IllegalArgumentException(
					"Weight cannot be negative or zero. The passed parameter is illegal. weightInKg: " + weightInKg);
		}
		if (heightInMeter <= 0) {
			throw new IllegalArgumentException(
					"Height cannot be negative or zero. The passed parameter is illegal. heightInMeter: "
							+ heightInMeter);
		}
		return (weightInKg / (heightInMeter * heightInMeter));
	}

	/**
	 * Calculates BMI value from weight and height using US units (pounds and
	 * inches).
	 * 
	 * @param weightInPounds
	 *            Body weight in pounds.
	 * @param heightInInches
	 *            Body height in inches.
	 * @return The calculated BMI value = (weight converted to kilograms) /
	 *         ((height converted to meters) * (height converted to meters))
	 * @throws IllegalArgumentException
	 *             If parameters are negative or zero.
	 */
	public static double calculateBMIFromUsInput(double weightInPounds, double heightInInches)
			throws IllegalArgumentException {
		if (weightInPounds <= 0) {
			throw new IllegalArgumentException(
					"Weight cannot be negative or zero. The passed parameter is illegal. weightInPounds: "
							+ weightInPounds);
		}
		if (heightInInches <= 0) {
			throw new IllegalArgumentException(
					"Height cannot be negative or zero. The passed parameter is illegal. heightInInches: "
							+ heightInInches);
		}
		return calculateBMI(convertPoundsToKilograms(weightInPounds), convertInchesToMeters(heightInInches));
	}

	/**
	 * Static helper method to convert from US to metric units.
	 * 
	 * @param valueInPounds
	 *            The value in pounds to be converted.
	 * @return The value in kilograms converted from pounds.
	 */
	private static double convertPoundsToKilograms(double valueInPounds) {
		return valueInPounds * ONE_POUND_IN_KILOGRAMS;
	}

	/**
	 * Static helper method to convert from US to metric units.
	 * 
	 * @param valueinInches
	 *            The value in inches to be converted.
	 * @return The value in meters converted from inches.
	 */
	private static double convertInchesToMeters(double valueinInches) {
		return valueinInches * ONE_INCH_IN_METERS;
	}
}
